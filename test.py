from django.test import RequestFactory, TestCase
from django.contrib.auth.models import User
from app_tp2.models import Estoque
from app_tp2.views import EstoqueForm, InserirProduto, RemoverProduto, AtualizarProduto 

class DataBaseTest(TestCase):
    # Create a user of test
    def setUp(self):
        self.credentials = {
            'username': 'test',
            'password': '123456'}
        user = User.objects.create_user(**self.credentials)
        user.save()

    # Make login on the site and when it make login redirec to the user page
    # So this also count as test to the filter form 
    def test_login(self):
        response = self.client.post('/login', self.credentials, follow=True)
        self.assertTrue(response.context['user'].is_active)
        # print(response.redirect_chain)

    
    def test_database(self):
        # Make Login

        response = self.client.post('/login', self.credentials, follow=True)
        self.assertTrue(response.context['user'].is_active)
        print("- Login OK")

        # Teste form insertion
        response = self.client.post('/inserir', { "nome":"Vaca", "preço":1, "descricao":"Faz Muu", "quantidade":100}, follow=True )
        self.assertEqual(response.status_code, 200)
        print("- Inserir OK")
        
        response = self.client.get('/editar/1')
        self.assertEqual(response.status_code, 301)
        print("- Editar OK")
        
    
        response = self.client.get('/detalhar/1')
        self.assertEqual(response.status_code, 301)
        print("- Detalhar OK")

        
        response = self.client.get('/remover/1')
        self.assertEqual(response.status_code, 301)
        print("- Remover OK")